package com.example.miniprojecthospitalkelompok2.dto;

import lombok.Data;

@Data
public class SignUpRequestDTO {

    private String userName;

    private String email;

    private String password;

    private String role;


}
