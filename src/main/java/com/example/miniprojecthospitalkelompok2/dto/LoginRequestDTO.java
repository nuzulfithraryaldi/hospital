package com.example.miniprojecthospitalkelompok2.dto;

import lombok.Data;

@Data
public class LoginRequestDTO {
    private String userName;
    private String password;
}
